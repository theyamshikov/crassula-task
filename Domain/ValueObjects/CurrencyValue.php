<?php

namespace App\Domain\ValueObjects;

class CurrencyValue
{

    private $code;
    private $amount;

    /**
     * Объект-значение неизменен, поэтому значения передаем ТОЛЬКО через конструктор
     *
     * @param string $code
     * @param float $amount
     */
    public function __construct(string $code, float $amount)
    {
        $this->code = $code;
        $this->amount = $amount;
    }

    public function getCode(): string
    {
        return $this->code;
    }

    public function getAmount(): float
    {
        return $this->amount;
    }
}
