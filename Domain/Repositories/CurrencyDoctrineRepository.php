<?php

namespace App\Domain\Repositories;

use App\Domain\Entities\CurrencyEntity;
use App\Domain\Interfaces\CurrencyStoreRepositoryInterface;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Criteria;

/**
 * Реализация репозитория для постоянного хранения курсов валют с помощью Doctrine
 */
class CurrencyDoctrineRepository implements CurrencyStoreRepositoryInterface
{

    public function findAll(Criteria $criteria = null): Collection
    {
        // TODO: Implement findAll() method.
    }

    public function findOneByCode(string $code): CurrencyEntity
    {
        // TODO: Implement findOneByCode() method.
    }

    public function findOneById(int $id): CurrencyEntity
    {
        // TODO: Implement findOneById() method.
    }

    public function insert(CurrencyEntity $currencyEntity)
    {
        // TODO: Implement insert() method.
    }

    public function update(CurrencyEntity $currencyEntity)
    {
        // TODO: Implement update() method.
    }

    public function delete(CurrencyEntity $currencyEntity)
    {
        // TODO: Implement delete() method.
    }
}
