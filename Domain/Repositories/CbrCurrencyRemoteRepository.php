<?php

namespace App\Domain\Repositories;

use App\Domain\Interfaces\CurrencyRemoteRepositoryInterface;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Criteria;

/**
 * Репозиторий для доступа к курсам валют из CBR
 */
class CbrCurrencyRemoteRepository implements CurrencyRemoteRepositoryInterface
{

    public function findAll(Criteria $criteria = null): Collection
    {
        // TODO: Implement findAll() method.
    }
}
