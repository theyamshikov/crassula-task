<?php

namespace App\Domain\Forms;

use Symfony\Component\Validator\Mapping\ClassMetadata;

/**
 * Форма конвертации валют
 */
class CalculateForm
{

    private $fromCurrencyCode;
    private $toCurrencyCode;
    private $amount;

    public static function loadValidatorMetadata(ClassMetadata $metadata)
    {

    }

    public function getFromCurrencyCode()
    {
        return $this->fromCurrencyCode;
    }

    public function setFromCurrencyCode($fromCurrencyCode): void
    {
        $this->fromCurrencyCode = $fromCurrencyCode;
    }

    public function getToCurrencyCode()
    {
        return $this->toCurrencyCode;
    }

    public function setToCurrencyCode($toCurrencyCode): void
    {
        $this->toCurrencyCode = $toCurrencyCode;
    }

    public function getAmount()
    {
        return $this->amount;
    }

    public function setAmount($amount): void
    {
        $this->amount = $amount;
    }
}
