<?php

namespace App\Domain\Interfaces;

/**
 * Сервис для синхронизации курсов валют с внешним источником
 */
interface CurrencySyncServiceInterface
{

    /**
     * Синхронизация курсов валют с внешним источником
     */
    public function sync(): void;
}
