<?php

namespace App\Domain\Interfaces;

use App\Domain\Entities\CurrencyEntity;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Criteria;

/**
 * Репозиторий для постоянного хранения курсов валют
 */
interface CurrencyStoreRepositoryInterface
{

    /**
     * Получить все курсы валют
     *
     * @param Criteria|null $criteria Условие выборки
     * @return Collection | CurrencyEntity[]
     */
    public function findAll(Criteria $criteria = null): Collection;

    /**
     * Получить курс валюты по ее коду
     *
     * @param string $code
     * @return CurrencyEntity
     */
    public function findOneByCode(string $code): CurrencyEntity;

    /**
     * Получить курс валюты по ее ID
     *
     * @param int $id
     * @return CurrencyEntity
     */
    public function findOneById(int $id): CurrencyEntity;

    /**
     * Добавить курс валюты
     *
     * @param CurrencyEntity $currencyEntity
     * @return mixed
     */
    public function insert(CurrencyEntity $currencyEntity);

    /**
     * Обновить курс валюты
     *
     * @param CurrencyEntity $currencyEntity
     * @return mixed
     */
    public function update(CurrencyEntity $currencyEntity);

    /**
     * Удалить курс валюты
     *
     * @param CurrencyEntity $currencyEntity
     * @return mixed
     */
    public function delete(CurrencyEntity $currencyEntity);
}
