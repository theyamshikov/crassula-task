<?php

namespace App\Domain\Interfaces;

use App\Domain\Entities\CurrencyEntity;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Criteria;

/**
 * Репозиторий внешнего источника курсов валют
 */
interface CurrencyRemoteRepositoryInterface
{

    /**
     * Получить все курсы валют
     *
     * @param Criteria|null $criteria
     * @return Collection | CurrencyEntity[]
     */
    public function findAll(Criteria $criteria = null): Collection;
}
