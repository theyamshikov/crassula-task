<?php

namespace App\Domain\Interfaces;

use App\Domain\Forms\CalculateForm;
use App\Domain\ValueObjects\CurrencyValue;

/**
 * Калькулятор курсов валют
 */
interface CurrencyCalculatorServiceInterface
{

    /**
     * Конвертация валют
     *
     * @param CalculateForm $calculateForm
     * @return CurrencyValue
     */
    public function calculate(CalculateForm $calculateForm): CurrencyValue;
}
