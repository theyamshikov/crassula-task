<?php

namespace App\Domain\Services;

use App\Domain\Forms\CalculateForm;
use App\Domain\Interfaces\CurrencyCalculatorServiceInterface;
use App\Domain\Interfaces\CurrencyStoreRepositoryInterface;
use App\Domain\ValueObjects\CurrencyValue;

class CurrencyCalculatorService implements CurrencyCalculatorServiceInterface
{

    private $currencyStoreRepository;

    public function __construct(CurrencyStoreRepositoryInterface $currencyStoreRepository)
    {
        $this->currencyStoreRepository = $currencyStoreRepository;
    }

    public function calculate(CalculateForm $calculateForm): CurrencyValue
    {
        $fromCurrencyEntity = $this->currencyStoreRepository->findOneByCode($calculateForm->getFromCurrencyCode());
        $toCurrencyEntity = $this->currencyStoreRepository->findOneByCode($calculateForm->getToCurrencyCode());
        $rate = $fromCurrencyEntity->getRate() / $toCurrencyEntity->getRate();
        $amount = $rate * $calculateForm->getAmount();
        return new CurrencyValue($toCurrencyEntity->getCode(), $amount);
    }
}
