<?php

namespace App\Domain\Services;

use App\Domain\Interfaces\CurrencyRemoteRepositoryInterface;
use App\Domain\Interfaces\CurrencyStoreRepositoryInterface;
use App\Domain\Interfaces\CurrencySyncServiceInterface;
use ZnDomain\Entity\Exceptions\AlreadyExistsException;

class CurrencySyncService implements CurrencySyncServiceInterface
{

    private $currencyRemoteRepository;
    private $currencyStoreRepository;

    public function __construct(CurrencyRemoteRepositoryInterface $currencyRemoteRepository, CurrencyStoreRepositoryInterface $currencyStoreRepository)
    {
        $this->currencyRemoteRepository = $currencyRemoteRepository;
        $this->currencyStoreRepository = $currencyStoreRepository;
    }

    public function sync(): void
    {
        // читаем из внешнего источника
        $collection = $this->currencyRemoteRepository->findAll();

        // сохраняем в БД
        foreach ($collection as $currencyEntity) {
            try {
                $this->currencyStoreRepository->insert($currencyEntity);
            } catch (AlreadyExistsException $exception) {
                $this->currencyStoreRepository->update($currencyEntity);
            }
        }
    }
}
