<?php

namespace App\Domain\Entities;

use Symfony\Component\Validator\Mapping\ClassMetadata;

/**
 * Сущность курса валюты
 */
class CurrencyEntity
{

    private $id;
    private $code;
    private $rate;

    public static function loadValidatorMetadata(ClassMetadata $metadata)
    {

    }

    public function getId()
    {
        return $this->id;
    }

    public function setId($id): void
    {
        $this->id = $id;
    }

    public function getCode()
    {
        return $this->code;
    }

    public function setCode($code): void
    {
        $this->code = $code;
    }

    public function getRate()
    {
        return $this->rate;
    }

    public function setRate($rate): void
    {
        $this->rate = $rate;
    }
}
