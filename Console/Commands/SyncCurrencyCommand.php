<?php

namespace App\Console\Commands;

use App\Domain\Interfaces\CurrencySyncServiceInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class SyncCurrencyCommand extends Command
{
    protected static $defaultName = 'app:currency:sync';
    protected static $defaultDescription = 'Синхронизация курсов валют';

    private $currencySyncService;

    public function __construct(CurrencySyncServiceInterface $currencySyncService)
    {
        $this->currencySyncService = $currencySyncService;

        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->currencySyncService->sync();
        return 0;
    }
}
