<?php

namespace App\Api\Controllers;

use App\Domain\Forms\CalculateForm;
use App\Domain\Interfaces\CurrencyCalculatorServiceInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class CurrencyController
{

    private $currencySyncService;

    public function __construct(CurrencyCalculatorServiceInterface $currencySyncService)
    {
        $this->currencySyncService = $currencySyncService;
    }

    public function calculate(Request $request): Response
    {
        $form = new CalculateForm();
        $buildForm = $this->buildForm($form, $request);
        if ($buildForm->isSubmitted() && $buildForm->isValid()) {
            $value = $this->currencySyncService->calculate($form);
        }
        return new JsonResponse([
            'code' => $value->getCode(),
            'amount' => $value->getAmount(),
        ]);
    }
}